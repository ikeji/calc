function noOverwrapUnion(nodes) {
  var shift = 0;
  var rv = [];
  for(var i=0;i<nodes.length;i++){
    rv[i] = nodes[i].translate({y: shift});
    var s = getNodeSize(rv[i]);
    shift += s[1] + 10;
  }
  return union(rv);
};

function empty() {
  return union([]);
}

var DEBUG = false;
// var DEBUG = true;

function d(a) {
  if (DEBUG) {
    return a;
  } else {
    return empty();
  }
}

function m3hole() {
  if(DEBUG) {
    return union([
      arc({end:360}).scale(6/2),  // nut
      arc({end:360}).scale(3.5/2),
    ]);
  } else {
    return arc({end:360}).scale(3.5/2);
  }
}


var BOARD_WIDTH = 90;
var BOARD_HEIGHT = 150;
var BOARD_MARGIN = 10;
var DISPLAY_HOLE_MARGIN = 1;
var DISPLAY_HOLE_WIDTH = 71 + DISPLAY_HOLE_MARGIN;
var DISPLAY_HOLE_HEIGHT = 25 + DISPLAY_HOLE_MARGIN;
var DISPLAY_HOLE_LEFT = 6 + 2.5;
var DISPLAY_HOLE_TOP = 3.5 + 3;
var DISPLAY_BOARD_WIDTH = 85;
var DISPLAY_BOARD_HEIGHT = 32.5;
var DISPLAY_BOARD_LEFT = 2.5;
var DISPLAY_BOARD_TOP = 3;
var DISPLAY_NOTCH_SHIFT = 3;
var BACK_SIZE = 19;
var KEY_SIZE = 16;
var KEY_SKIP = 20;
var KEY_TOP = 40;
var KEY_LEFT = 5;
var TACT_SIZE = 5;
var MOUNT_LEFT = 3.3;
var MOUNT_TOP = 60;
var MOUNT_BOTTOM = 5;

function display() {
  return union([
    square({width: DISPLAY_HOLE_WIDTH, height: DISPLAY_HOLE_HEIGHT})
    .translate({x: DISPLAY_HOLE_LEFT, y: DISPLAY_HOLE_TOP}),
    d(square({width: DISPLAY_BOARD_WIDTH, height: DISPLAY_BOARD_HEIGHT})
    .translate({x: DISPLAY_BOARD_LEFT, y: DISPLAY_BOARD_TOP})),
    arc({end:360}).scale(1.5)
    .translate({
      x: DISPLAY_BOARD_LEFT + DISPLAY_NOTCH_SHIFT,
      y: DISPLAY_BOARD_TOP,
    }),
    arc({end:360}).scale(1.5)
    .translate({
      x: DISPLAY_BOARD_LEFT + DISPLAY_BOARD_WIDTH - DISPLAY_NOTCH_SHIFT,
      y: DISPLAY_BOARD_TOP,
    }),
    arc({end:360}).scale(1.5)
    .translate({
      x: DISPLAY_BOARD_LEFT + DISPLAY_NOTCH_SHIFT,
      y: DISPLAY_BOARD_TOP + DISPLAY_BOARD_HEIGHT,
    }),
    arc({end:360}).scale(1.5)
    .translate({
      x: DISPLAY_BOARD_LEFT + DISPLAY_BOARD_WIDTH - DISPLAY_NOTCH_SHIFT,
      y: DISPLAY_BOARD_TOP + DISPLAY_BOARD_HEIGHT,
    }),
  ]);
}

function key(cap) {
  var key = arc({end:360}).scale(KEY_SIZE/2);
  var back = arc({end:360}).scale(BACK_SIZE/2);
  return union([
    DEBUG || cap ? key : empty(),
    DEBUG || !cap ? back : empty(),
    d(arc({end:360}).scale(TACT_SIZE/2)),
  ]);
}

function keys(cap) {
  var rv = [];
  for (var x = 0;x<1;x++) {
    for (var y = 0;y<5;y++) {
      rv.push(key(cap).translate({
        x: KEY_LEFT + KEY_SKIP*x + KEY_SKIP/2,
        y: KEY_TOP + KEY_SKIP*y + KEY_SKIP/2,
      }));
    }
  }
  return union(rv);
}

function holes() {
  return union([
    m3hole()
    .translate({
      x: MOUNT_LEFT,
      y: MOUNT_TOP,
    }),
    m3hole()
    .translate({
      x: MOUNT_LEFT,
      y: BOARD_HEIGHT - MOUNT_BOTTOM,
    }),
    m3hole()
    .translate({
      x: BOARD_WIDTH - MOUNT_LEFT,
      y: MOUNT_TOP,
    }),
    m3hole()
    .translate({
      x: BOARD_WIDTH - MOUNT_LEFT,
      y: BOARD_HEIGHT - MOUNT_BOTTOM,
    }),
  ]);
}

function board(contents) {
  return union([
    roundedSquare({
      width: BOARD_WIDTH+BOARD_MARGIN*2,
      height: BOARD_HEIGHT+BOARD_MARGIN*2,
      round: BOARD_MARGIN,
    }),
    contents.translate({x: BOARD_MARGIN, y:BOARD_MARGIN}),
  ]);
}

function topBoard() {
  return board(
    union([
      d(square({
        width: BOARD_WIDTH,
        height: BOARD_HEIGHT,
      })),
      display(),
      keys(true),
      holes(),
    ]));
}

function bottomBoard() {
  return board(
    union([
      holes(),
    ]));
}


function main() {
  return noOverwrapUnion([
    // topBoard(),
    // keys(false).translate({y: - KEY_TOP}),
    //bottomBoard(),
    keys(true).translate({y: - KEY_TOP}),
  ]).translate({x:10,y:10});
}
