#include "marut.h"

#ifdef __unix__
#define CONSOLE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef CONSOLE

void error(const char* msg) {
  printf("%s", msg);
  system ("/bin/stty cooked");
  abort();
}

int main() {
  system ("/bin/stty raw");
  initialize();
  while(1) {
    eval();
    updateBuffer();
    updateDisplay();
  }
}

void newline() {
  printf("\r\n");
}

#endif

// Keyboard Scanner {{{

const char keymap[][4] = {
  { ' ', ' ', ' ', '$', },
  { '7', '8', '9', '/', },
  { '4', '5', '6', '*', },
  { '1', '2', '3', '-', },
  { '0', '.', ' ', '+', },
};
const unsigned char bit2pos[] = {
  0-1, // 00000000
  1-1, // 00000001
  2-1, // 00000010
  0-1, // 00000011
  3-1, // 00000100
  0-1, // 00000101
  0-1, // 00000110
  0-1, // 00000111
  4-1, // 00001000
  0-1, // 00001001
  0-1, // 00001010
  0-1, // 00001011
  0-1, // 00001100
  0-1, // 00001101
  0-1, // 00001110
  0-1, // 00001111
  8-1, // 00010000
  0-1, // 00010001
  0-1, // 00010010
  0-1, // 00010011
  0-1, // 00010100
  0-1, // 00010101
  0-1, // 00010110
  0-1, // 00010111
  0-1, // 00011000
  0-1, // 00011001
  0-1, // 00011010
  0-1, // 00011011
  0-1, // 00011100
  0-1, // 00011101
  0-1, // 00011110
  0-1, // 00011111
};

unsigned char prev[5] = { 0,0,0,0,0 };

int scan() {
  while(1) {
    for(int i=0;i<5;i++) {
#ifdef CONSOLE
      unsigned char v = 0;
#else
      unsigned char v = getRow(i);
#endif
      prev[i] = prev[i] & v;
      v = v & ~prev[i];
      if (v != 0 && v < 17 && bit2pos[v] >= 0) {
        prev[i] = v;
        return keymap[i][bit2pos[v]];
      }
    }
  }
}

// }}}
// Keyboard Mapper {{{

char input_buf[256];
int input_buf_cur = 0;

void push_key(char c) {
  input_buf[input_buf_cur] = c;
  input_buf_cur++;
}

void push_keys(char* c) {
  while (*c!='\0') {
    input_buf[input_buf_cur] = *c;
    input_buf_cur++;
    c++;
  }
}

void input() {
#ifdef CONSOLE
  char c = getchar();
  if (c == EOF) error("EOF");
  if (c == '!') error("QUIT");
#else
  char c = scan();
#endif
  if (
      ('a' <= c && c <= 'z') ||
      ('A' <= c && c <= 'Z') ||
      ('0' <= c && c <= '9') ||
      (c == ' ') || (c == '.') ||
      0) {
    push_key(c);
  } else if (c == '+') {
    push_keys(" PLUS ");
  } else if (c == '-') {
    push_keys(" MINUS ");
  } else if (c == '*') {
    push_keys(" MUL ");
  } else if (c == '/') {
    push_keys(" DIV ");
  } else if (c == '$') {
    push_keys(" DROP ");
  }
}

char peek_key() {
  while (input_buf_cur == 0) input();
  return input_buf[0];
}

void next_key() {
  while (input_buf_cur == 0) input();
  memmove(input_buf, input_buf+1, input_buf_cur-1);
  input_buf_cur--;
}

// }}}
// Tokeniser {{{

TokenType getToken(char* buf, int buflen) {
  int wt = 0;
  TokenType state = UNKNOWN_TOKEN;
  buf[0] = 0;
  while(1) {
    char c = peek_key();
    if (state == UNKNOWN_TOKEN) {
      if ('a' <= c && c <= 'z') {
        state = SYMBOL_TOKEN;
      } else if ('A' <= c && c <= 'Z') {
        state = SYMBOL_TOKEN;
      } else if ('0' <= c && c <= '9') {
        state = INTEGER_TOKEN;
      } else if ('.' == c) {
        state = FLOAT_TOKEN;
      }
    } else if (state == INTEGER_TOKEN) {
      if ('0' <= c && c <= '9') {
      } else if ('.' == c) {
        state = FLOAT_TOKEN;
      } else {
        return state;
      }
    } else if (state == FLOAT_TOKEN) {
      if ('0' <= c && c <= '9') {
      } else {
        return state;
      }
    } else if (state == SYMBOL_TOKEN) {
      if ('a' <= c && c <= 'z') {
      } else if ('A' <= c && c <= 'Z') {
      } else {
        return state;
      }
    }
    if (wt < 10 && state != UNKNOWN_TOKEN) {
      buf[wt] = c;
      wt++;
      buf[wt] = '\0';
    }
    next_key();
    setWorkLine(buf);
  }
}

// }}}
// Parser {{{

void getNode(Node* n) {
  char buf[256];
  TokenType t = getToken(buf, 256);
  n->type = t;
  switch(t) {
    case INTEGER_TOKEN:
      n->intValue = atoi(buf);
      break;
    case FLOAT_TOKEN:
      n->doubleValue = strtod(buf, 0);
      break;
    case SYMBOL_TOKEN:
      strncpy(n->symbol, buf, 256);
      break;
    case UNKNOWN_TOKEN:
      // TODO:
      break;
  }
}

// }}}
// Evaluator {{{

void eval() {
  Node buf;
  getNode(&buf);
  switch(buf.type) {
    case INTEGER_TOKEN:
      pushInt(buf.intValue);
      break;
    case FLOAT_TOKEN:
      pushDouble(buf.doubleValue);
      break;
    case SYMBOL_TOKEN:
      {
        Fun* f = lookupDict(buf.symbol);
        // TODO: handle error.
        if (f != 0) {
          f->fn();
        }
      }
      break;
    case UNKNOWN_TOKEN:
      // TODO: 
      break;
  }
}

// }}}
// Stack {{{

Value stack[256];
int top = 0;

void push(Value val) {
  stack[top] = val;
  top++;
}
void pushInt(int val) {
  push((Value){.type = INTEGER, .intValue = val});
}
void pushDouble(double val) {
  push((Value){.type = DOUBLE, .doubleValue = val});
}
Value pop() {
  if (top == 0) return (Value){UNKNOWN};
  top--;
  return stack[top];
}
Value peek() {
  if (top == 0) return (Value){UNKNOWN};
  return stack[top-1];
}
Value peek2() {
  if (top < 2) return (Value){UNKNOWN};
  return stack[top-2];
}

// }}}
// Dict {{{

Fun dict[20];
int dict_top = 0;

void addToDict(Fun fn) {
  dict[dict_top] = fn;
  dict_top++;
}
Fun* lookupDict(const char* name) {
  for (int i=0;i<dict_top;i++) {
    Fun* f = &dict[i];
    if (!strcmp(name, f->name) &&
        top >= f->numArg) {
      char match = 0;
      for (int i=0;i<f->numArg;i++) {
        if (!(f->targ[i] == ANY ||
              stack[top-1-i].type == f->targ[i])) {
          match = 1;
          break;
        }
      }
      if (match == 1) continue;
      return f;
    }
  }
  return 0;
}

// }}}
// Stdlib {{{

void ii_plus_fn() {
  int a = pop().intValue;
  int b = pop().intValue;
  pushInt(a+b);
}
void di_plus_fn() {
  int a = pop().intValue;
  double b = pop().doubleValue;
  pushDouble(a+b);
}
void id_plus_fn() {
  double a = pop().doubleValue;
  int b = pop().intValue;
  pushDouble(a+b);
}
void dd_plus_fn() {
  double a = pop().doubleValue;
  double b = pop().doubleValue;
  pushDouble(a+b);
}
void ii_minus_fn() {
  int a = pop().intValue;
  int b = pop().intValue;
  pushInt(b-a);
}
void di_minus_fn() {
  int a = pop().intValue;
  double b = pop().doubleValue;
  pushDouble(b-a);
}
void id_minus_fn() {
  double a = pop().doubleValue;
  int b = pop().intValue;
  pushDouble(b-a);
}
void dd_minus_fn() {
  double a = pop().doubleValue;
  double b = pop().doubleValue;
  pushDouble(b-a);
}
void ii_mul_fn() {
  int a = pop().intValue;
  int b = pop().intValue;
  pushInt(a*b);
}
void di_mul_fn() {
  int a = pop().intValue;
  double b = pop().doubleValue;
  pushDouble(a*b);
}
void id_mul_fn() {
  double a = pop().doubleValue;
  int b = pop().intValue;
  pushDouble(a*b);
}
void dd_mul_fn() {
  double a = pop().doubleValue;
  double b = pop().doubleValue;
  pushDouble(a*b);
}
void ii_div_fn() {
  int a = pop().intValue;
  int b = pop().intValue;
  if (b % a == 0) {
    pushInt(b/a);
  } else {
    pushDouble(((double)b)/a);
  }
}
void di_div_fn() {
  int a = pop().intValue;
  double b = pop().doubleValue;
  pushDouble(b/a);
}
void id_div_fn() {
  double a = pop().doubleValue;
  int b = pop().intValue;
  pushDouble(b/a);
}
void dd_div_fn() {
  double a = pop().doubleValue;
  double b = pop().doubleValue;
  pushDouble(b/a);
}
void drop_fn() {
  pop();
}

void initializeDict(){
  addToDict((Fun){"PLUS",ii_plus_fn,2,{INTEGER, INTEGER}});
  addToDict((Fun){"PLUS",di_plus_fn,2,{INTEGER, DOUBLE}});
  addToDict((Fun){"PLUS",id_plus_fn,2,{DOUBLE, INTEGER}});
  addToDict((Fun){"PLUS",dd_plus_fn,2,{DOUBLE, DOUBLE}});
  addToDict((Fun){"MINUS",ii_minus_fn,2,{INTEGER, INTEGER}});
  addToDict((Fun){"MINUS",di_minus_fn,2,{INTEGER, DOUBLE}});
  addToDict((Fun){"MINUS",id_minus_fn,2,{DOUBLE, INTEGER}});
  addToDict((Fun){"MINUS",dd_minus_fn,2,{DOUBLE, DOUBLE}});
  addToDict((Fun){"MUL",ii_mul_fn,2,{INTEGER, INTEGER}});
  addToDict((Fun){"MUL",di_mul_fn,2,{INTEGER, DOUBLE}});
  addToDict((Fun){"MUL",id_mul_fn,2,{DOUBLE, INTEGER}});
  addToDict((Fun){"MUL",dd_mul_fn,2,{DOUBLE, DOUBLE}});
  addToDict((Fun){"DIV",ii_div_fn,2,{INTEGER, INTEGER}});
  addToDict((Fun){"DIV",di_div_fn,2,{INTEGER, DOUBLE}});
  addToDict((Fun){"DIV",id_div_fn,2,{DOUBLE, INTEGER}});
  addToDict((Fun){"DIV",dd_div_fn,2,{DOUBLE, DOUBLE}});
  addToDict((Fun){"DROP",drop_fn,1,{ANY}});
}

// }}}
// Window {{{

char workline[256];

void setWorkLine(char* buf) {
  strncpy(workline, buf, 256);
  updateBuffer();
  updateDisplay();
}

// }}}
// Display {{{

char line1[17];
char line2[17];
const char* empty = "                 ";

void formatValue(char* buf, Value v) {
  switch(v.type) {
    case INTEGER:
      sprintf(buf, "%d", v.intValue);
      break;
    case DOUBLE:
      sprintf(buf, "%5lf", v.doubleValue);
      break;
    default:
      sprintf(buf, "err");
      break;
  }
}

void updateBuffer() {
  if (strlen(workline) == 0) {
    if (top > 0) {
      char buf[256];
      formatValue(buf, peek());
      sprintf(line1," %12s %2d", buf, top);
    } else {
      sprintf(line1,"              %2d", top);
    }
    if (top > 1) {
      char buf[256];
      formatValue(buf, peek2());
      sprintf(line2," %12s", buf);
    } else {
      strcpy(line2, empty);
    }
  } else {
    sprintf(line1,">%12s   ", workline);
    if (top > 0) {
      char buf[256];
      formatValue(buf, peek());
      sprintf(line2," %12s %2d", buf, top);
    } else {
      sprintf(line2,"              %2d", top);
    }
  }
}

#ifdef CONSOLE
void updateDisplay() {
  newline();
  printf("--------");
  newline();
  printf("%s", line1);
  newline();
  printf("%s", line2);
  newline();
  printf("--------");
  newline();
}
#endif

// }}}

void initialize() {
  workline[0] = 0;
  initializeDict();
}
