#ifdef __cplusplus
extern "C" {
#endif

// Keyboard Scanner {{{

unsigned char getRow(unsigned char row);
int scan();

// }}}
// Keyboard Mapper {{{

char peek_key();
void next_key();

// }}}
// Tokeniser {{{

typedef enum {
  UNKNOWN_TOKEN,
  INTEGER_TOKEN,
  FLOAT_TOKEN,
  SYMBOL_TOKEN,
} TokenType;
TokenType getToken();

// }}}
// Parser {{{

typedef struct {
  TokenType type;
  union {
    int intValue;
    double doubleValue;
    char symbol[256];
  };
} Node;
void getNode(Node* buf);

// }}}
// Evaluator {{{

void eval();

// }}}
// Stack {{{

typedef enum {
  UNKNOWN,
  ANY,
  INTEGER,
  DOUBLE,
} ValueType;

typedef struct {
  ValueType type;
  union {
    int intValue;
    double doubleValue;
  };
} Value;

void push(Value val);
void pushInt(int val);
void pushDouble(double val);
Value pop();
Value peek();
Value peek2();

// }}}
// Dict {{{

typedef void Fn();
typedef struct {
  const char* name;
  Fn* fn;
  int numArg;
  ValueType targ[10];
} Fun;

void addToDict(Fun fn);
Fun* lookupDict(const char* name);

// }}}
// Stdlib {{{
//
void initializeDict();

// }}}
// Window {{{

void setWorkLine(char* buf);

// }}}
// Display {{{

void updateBuffer();
void updateDisplay();

// }}}

void initialize();

#ifdef __cpluscplus
}
#endif
