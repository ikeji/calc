
#include <LiquidCrystal.h>


const int rs = PA5, en = PA4, d4 = PA3, d5 = PA2, d6 = PA1, d7 = PA0;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

const byte num_pins = 5;
const int key_pins[] = {
  PB5,
  PB6,
  PB7,
  PB8,
  PB9,
};

extern "C" void initialize();
extern "C" void eval();
extern "C" void updateBuffer();
extern "C" void updateDisplay();
extern "C" unsigned char getRow(unsigned char row);


void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  for (byte r = 0; r < num_pins; r++) {
    pinMode(key_pins[r], INPUT_PULLUP);
  }
  initialize();
  Serial.println("booted");
  updateBuffer();
  updateDisplay();
}

void loop() {
  eval();
  updateBuffer();
  updateDisplay();
}

extern "C" unsigned char getRow(unsigned char row) {
  pinMode(key_pins[row], OUTPUT);
  digitalWrite(key_pins[row], LOW);
  delay(3);

  byte rv = 0;
  for (int i=0;i<num_pins;i++) {
    if (i==row) continue;
    rv <<= 1;
    rv += (digitalRead(key_pins[i]) ? 0 : 1);
  }
  pinMode(key_pins[row], INPUT_PULLUP);
  return rv;
}

extern char line1[17];
extern char line2[17];

extern "C" void updateDisplay() {
  Serial.println("--------");
  Serial.println(line1);
  Serial.println(line2);
  Serial.println("--------");
  lcd.setCursor(0, 0);
  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
}
